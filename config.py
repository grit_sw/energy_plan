import os


class Config(object):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SECRET_KEY = os.environ['SECRET_KEY']
    ADMINS = ['workshop@grit.systems', ]
    INVITE_EXPIRES = 600

    @staticmethod
    def init_app(app):
        pass


class Development(Config):
    DEBUG = True

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)

    @staticmethod
    def init_app(app, *args):
        pass


class Testing(Config):
    """TODO"""
    pass


class Staging(Config):
    """TODO"""
    pass

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


class Production(Config):
    """TODO"""

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)


config = {
    'Development': Development,
    'Testing': Testing,
    'Production': Production,
    'Staging': Staging,

    'default': Development,
}
