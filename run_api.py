# Test Server.
from api import create_api, db
from os import environ
from flask_migrate import Migrate
from logger import logger
from flask import request
from arrow import now


if environ.get('FLASK_ENV') is None:
    print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'Staging'
app = create_api(mode)
migrate = Migrate(app, db)


@app.after_request
def log_info(response):
    try:
        log_data = {
            'connection': request.headers.get('Connection'),
            'ip_address': request.remote_addr,
            'browser_name': request.user_agent.browser,
            'user_device': request.user_agent.platform,
            'referrer': request.referrer,
            'request_url': request.url,
            'host_url': request.host_url,
            'status_code': response.status_code,
            'date': str(now('Africa/Lagos')),
            'location': response.location,
        }
        logger.info('users_api_logs : {}'.format(log_data))
    except Exception as e:
        logger.exception("users_api_logs: {}".format(e))
        pass
    return response


with app.app_context():
    # db.reflect()
    # db.drop_all()
    db.create_all()
