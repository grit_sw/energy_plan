from flask import request
from flask_restplus import Namespace, Resource, fields
from api import api
from api.manager import EnergyPlanManager
from api.schema import NewPlanSchema, EditPlanSchema
from marshmallow import ValidationError
from logger import logger


energy_plan_api = Namespace('energy_plans', description='Api for managing energy plans')

# For swagger documentation
new_plan = energy_plan_api.model('new_plan', {
	'name': fields.String(required=True, description='The name of the plan'),
	'rate': fields.String(required=True, description='The price of energy in watt-hour'),
	'power_limit': fields.String(required=True, description='The power limit in VA'),
	'energy_limit': fields.String(required=True, description='The energy limit in Watts'),
	'facility_id': fields.String(required=True, description='The ID of the facility the plan is used in'),
})

edit_plan = energy_plan_api.model('edit_plan', {
	'name': fields.String(required=True, description='The name of the plan'),
	'rate': fields.String(required=True, description='The price of energy per kilowatt-hour'),
	'power_limit': fields.String(required=False, description='The power limit in VA'),
	'energy_limit': fields.String(required=False, description='The energy limit in Watts'),
	'plan_id': fields.String(required=True, description='The ID of the energy plan'),
	'facility_id': fields.String(required=True, description='The ID of the facility the plan is used in'),
})


@energy_plan_api.route('/new/')
class NewPlan(Resource):
	"""
		Api to credit a user's account
	"""

	@energy_plan_api.expect(new_plan)  # For swagger documentation
	def post(self):
		"""
			HTTP method to create one energy plan in a facility
			@param: request payload
			@returns: response and status code
		"""
		schema = NewPlanSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		response = {}

		try:
			payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = EnergyPlanManager()
		resp, code = manager.create_plan(payload)
		return resp, code


@energy_plan_api.route('/one/<string:plan_id>/<string:facility_id>/')
class ViewPlan(Resource):
	"""
		Api to credit a user's account
	"""

	def get(self, plan_id, facility_id):
		"""
			HTTP method to view one energy plan in a facility
			@param: request payload
			@returns: response and status code
		"""
		manager = EnergyPlanManager()
		resp, code = manager.one_plan(plan_id, facility_id)
		return resp, code


@energy_plan_api.route('/tx-engine/<string:plan_id>/')
class TXPlan(Resource):
	"""
		Api to credit a user's account
	"""

	def get(self, plan_id):
		"""
			HTTP method to view one energy plan in a facility
			@param: request payload
			@returns: response and status code
		"""
		manager = EnergyPlanManager()
		resp, code = manager.tx_energy_plan(plan_id)
		return resp, code


@energy_plan_api.route('/all/<string:facility_id>/')
class AllPlans(Resource):
	"""
		Api to credit a user's account
	"""

	def get(self, facility_id):
		"""
			HTTP method to view all energy plans
			@param: request payload
			@returns: response and status code
		"""
		manager = EnergyPlanManager()
		resp, code = manager.all_plans(facility_id)
		return resp, code


@energy_plan_api.route('/edit/')
class EditPlan(Resource):
	"""
		Api to credit a user's account
	"""

	@energy_plan_api.expect(edit_plan)  # For swagger documentation
	def post(self):
		"""
			HTTP method to edit one energy plan in a facility
			@param: request payload
			@returns: response and status code
		"""
		schema = EditPlanSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data

		response = {}

		try:
			payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		manager = EnergyPlanManager()
		resp, code = manager.edit_plan(**payload)
		return resp, code


@energy_plan_api.route('/delete/<string:plan_id>/<string:facility_id>/')
class DeletePlan(Resource):
	"""
		Api to credit a user's account
	"""

	def delete(self, plan_id, facility_id):
		"""
			HTTP method to delete one energy plan in a facility
			@param: request payload
			@returns: response and status code
		"""

		manager = EnergyPlanManager()
		resp, code = manager.delete_plan(plan_id, facility_id)
		return resp, code
