from api.models import EnergyPlan


class EnergyPlanManager(object):
	"""docstring for EnergyPlanManager"""

	def __init__(self):
		pass

	def create_plan(self, payload):
		response = {}
		name = payload['name']
		facility_id = payload['facility_id']

		plan = EnergyPlan.get_plan(facility_id=facility_id, name=name)
		if plan is not None:
			response['success'] = False
			response['message'] = 'Plan with name {} exists at this facility'.format(name)
			response['data'] = plan.one_response()
			return response, 409

		created, created_plan = EnergyPlan.create_plan(**payload)
		if not created:
			response['success'] = False
			response['message'] = 'Service Unavailable'
			return response, 500

		response['success'] = True
		response['message'] = 'Plan created'
		response['data'] = created_plan
		return response, 201

	def one_plan(self, plan_id, facility_id):
		response = {}
		plan = EnergyPlan.get_plan(plan_id=plan_id, facility_id=facility_id)
		if plan is None:
			response['success'] = False
			response['message'] = 'Plan not found'
			return response, 404
		response['success'] = True
		response['data'] = plan.one_response()
		return response, 200

	def all_plans(self, facility_id):
		response = {}
		plans = EnergyPlan.all_plans(facility_id)
		response['success'] = True
		response['data'] = plans
		return response, 200

	def edit_plan(self, name, rate, power_limit, energy_limit, plan_id, facility_id):
		response = {}
		plan = EnergyPlan.get_plan(plan_id=plan_id, facility_id=facility_id)
		if plan is None:
			response['success'] = False
			response['message'] = 'Plan not found'
			return response, 404

		edited, edited_plan = plan.edit_plan(name, rate, power_limit, energy_limit)
		if not edited:
			response['success'] = False
			response['message'] = 'Service Unavailable'
			return response, 404

		response['success'] = True
		response['message'] = 'Changes Saved'
		response['data'] = edited_plan
		return response, 201

	def delete_plan(self, plan_id, facility_id):
		response = {}
		plan = EnergyPlan.get_plan(plan_id=plan_id, facility_id=facility_id)
		if plan is None:
			response['success'] = False
			response['message'] = 'Plan not found'
			return response, 404

		deleted = plan.delete_plan()
		if not deleted:
			response['success'] = False
			response['message'] = 'Service Unavailable'
			return response, 500

		response['success'] = True
		response['message'] = 'Plan deleted'
		return response, 200
	
	def tx_energy_plan(self, energy_plan_id):
		response = {}
		plan = EnergyPlan.get_plan(plan_id=energy_plan_id)
		if plan is None:
			response['success'] = False
			response['message'] = 'Plan not found'
			return response, 404
		response['success'] = True
		response['data'] = plan.to_dict()
		return response, 200
