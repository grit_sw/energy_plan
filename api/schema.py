from marshmallow import ValidationError, Schema, fields, post_load, validates
from collections import namedtuple


NewPlan = namedtuple('NewPlan', [
	'name',
	'rate',
	'power_limit',
	'energy_limit',
	'facility_id',
])

EditPlan = namedtuple('EditPlan', [
	'name',
	'rate',
	'power_limit',
	'energy_limit',
	'plan_id',
	'facility_id',
])


class NewPlanSchema(Schema):
    name = fields.String(required=True)
    rate = fields.String(required=True)
    power_limit = fields.String(allow_none=True)
    energy_limit = fields.String(allow_none=True)
    facility_id = fields.String(required=True)

    @post_load
    def new_plan(self, data):
        return NewPlan(**data)

    @validates('name')
    def validate_name(self, value):
        try:
            value.title()
        except Exception:
            raise ValidationError('Invalid Input')

    @validates('rate')
    def validate_rate(self, value):
        try:
            float(value)
        except Exception:
            raise ValidationError('Invalid Entry')

    @validates('power_limit')
    def validate_power_limit(self, value):
        if value is not None:
            try:
                float(value)
            except Exception:
                raise ValidationError('Invalid Entry')

    @validates('energy_limit')
    def validate_energy_limit(self, value):
        if value is not None:
            try:
                float(value)
            except Exception:
                raise ValidationError('Invalid Entry')

    @validates('facility_id')
    def validate_facility_id(self, value):
        if len(value) <= 0:
            raise ValidationError('Facility required')

    def __repr__(self):
        return "NewPlanSchema <{}>".format(self.name)


class EditPlanSchema(Schema):
    name = fields.String(required=True)
    rate = fields.String(required=True)
    power_limit = fields.String(allow_none=True)
    energy_limit = fields.String(allow_none=True)
    plan_id = fields.String(required=True)
    facility_id = fields.String(required=True)

    @post_load
    def edit_plan(self, data):
        return EditPlan(**data)

    @validates('name')
    def validate_name(self, value):
        try:
            value.title()
        except Exception:
            raise ValidationError('Invalid Input')

    @validates('rate')
    def validate_rate(self, value):
        try:
            float(value)
        except Exception:
            raise ValidationError('Value must be a numerical object')

    @validates('facility_id')
    def validate_facility_id(self, value):
        if len(value) <= 0:
            raise ValidationError('Facility required')

    @validates('plan_id')
    def validate_plan_id(self, value):
        if len(value) <= 0:
            raise ValidationError('Plan required')

    def __repr__(self):
        return "EditPlanSchema <{}>".format(self.name)
