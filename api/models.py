from api import db
from uuid import uuid4
from logger import logger


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
							  onupdate=db.func.current_timestamp())


class EnergyPlan(Base):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	name = db.Column(db.String, unique=False, nullable=False)
	rate = db.Column(db.Float, unique=False, nullable=False)
	power_limit = db.Column(db.Float, unique=False, nullable=True)
	energy_limit = db.Column(db.Float, unique=False, nullable=True)
	plan_id = db.Column(db.String, unique=True, nullable=False)
	facility_id = db.Column(db.String, unique=False, nullable=False)

	def __init__(self, name, rate, plan_id, facility_id, power_limit=None, energy_limit=None):
		self.name = name
		self.rate = rate
		if power_limit is not None:
			self.power_limit = power_limit
		if energy_limit is not None:
			self.energy_limit = energy_limit
		self.plan_id = plan_id
		self.facility_id = facility_id

	def __repr__(self):
		return '<id: EnergyPlan {}'.format(self.plan_id)

	def create_plan(name, rate, power_limit, energy_limit, facility_id):
		created = False
		plan_id = str(uuid4())
		new_plan = EnergyPlan(
			name=name.title(),
			rate=rate,
			power_limit=power_limit,
			energy_limit=energy_limit,
			plan_id=plan_id,
			facility_id=facility_id,
		)
		db.session.add(new_plan)
		try:
			db.session.commit()
			db.session.refresh(new_plan)
			created = True
		except Exception as e:
			logger.exception(e)
			db.session.rollback()
		return created, new_plan.one_response()

	@staticmethod
	def get_plan(facility_id=None, plan_id=None, name=None):
		if not facility_id:
			plan = EnergyPlan.query.filter_by(plan_id=plan_id).first()
			return plan
		plan = EnergyPlan.query.filter_by(plan_id=plan_id).filter_by(facility_id=facility_id).first()
		if name is not None:
			name = name.title()
			plan = EnergyPlan.query.filter_by(name=name).filter_by(facility_id=facility_id).first()
		return plan

	@property
	def power_limit_is_set(self):
		if self.power_limit:
			return True
		return False

	@property
	def energy_limit_is_set(self):
		if self.energy_limit:
			return True
		return False

	def edit_plan(self, name, rate, power_limit, energy_limit):
		edited = False
		self.name = name.title()
		self.rate = rate
		self.power_limit = power_limit
		self.energy_limit = energy_limit
		db.session.add(self)
		try:
			db.session.commit()
			db.session.refresh(self)
			edited = True
		except Exception as e:
			logger.exception(e)
			db.session.rollback()
		return edited, self.one_response()

	def delete_plan(self):
		deleted = False
		db.session.delete(self)
		try:
			db.session.commit()
			deleted = True
		except Exception as e:
			logger.exception(e)
			db.session.rollback()
		return deleted

	@staticmethod
	def all_plans(facility_id):
		plans = EnergyPlan.query.filter_by(facility_id=facility_id).all()
		if len(plans) < 1:
			return []
		return [plan.to_dict() for plan in plans]

	def to_dict(self):
		resp = {
			'id': self.id,
			'name': self.name,
			'rate': self.rate,
			'power_limit': self.power_limit,
			'power_limit_is_set': self.power_limit_is_set,
			'energy_limit': self.energy_limit,
			'energy_limit_is_set': self.energy_limit_is_set,
			'plan_id': self.plan_id,
			'facility_id': self.facility_id,
			'date_created': str(self.date_modified),
		}
		return resp

	def one_response(self):
		resp = [self.to_dict()]
		return resp