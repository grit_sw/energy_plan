FROM alpine:edge

ENV FLASK_APP run_api.py


WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 postgresql-libs
RUN apk add --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev \
    libffi-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY migrations migrations
COPY config.py logger.py run_api.py start.sh ./
ENV FLASK_ENV Staging
RUN chmod +x start.sh
# run-time configuration
EXPOSE 5000
COPY api api
ENTRYPOINT ["./start.sh"]
